package sbu.cs.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public void readTwitterCommands(List<String> args) throws UnrecognizedCommandException, NotImplementedCommandException {
        List<String> commands = new ArrayList<>(Util.getNotImplementedCommands());
        for (String temp1 : args) {
            for (String temp2:commands) {
                if (temp1.equals(temp2)) {
                    throw new NotImplementedCommandException();
                }
            }
        }
        commands.addAll(Util.getImplementedCommands());
        outer : for (String temp1 : args) {
            for (String temp2:commands) {
                if (temp1.equals(temp2)) {
                    continue outer;
                }
            }
            throw new UnrecognizedCommandException();
        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String... args) throws BadInputException {
        for (int i = 1; i < args.length; i = i + 2) {
            try {
                Integer.parseInt(args[i]);
            }
            catch (NumberFormatException numberFormatException){
                    throw new BadInputException();
            }
        }
    }
}
