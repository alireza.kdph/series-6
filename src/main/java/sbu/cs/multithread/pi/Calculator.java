package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Calculator implements Runnable {

    int floatingPoint = PICalculator.getFloatingPoint() + 1;
    private MathContext mathContext = new MathContext(floatingPoint, RoundingMode.HALF_UP);
    private final BigDecimal SIXTEEN = new BigDecimal(16,mathContext);

    private final BigDecimal ONE = new BigDecimal(1);
    private final BigDecimal TWO = new BigDecimal(2);
    private final BigDecimal FOUR = new BigDecimal(4);
    private final BigDecimal FIVE = new BigDecimal(5);
    private final BigDecimal SIX = new BigDecimal(6);

    int k = 0;
    private final BigDecimal K8;
    public Calculator(int k) {
        this.k = k;
        K8 = new BigDecimal(k * 8);
    }



    @Override
    public void run() {
        MathContext mathContext= new MathContext(floatingPoint + 100, RoundingMode.HALF_DOWN);
        BigDecimal nth = new BigDecimal(0,mathContext);
        nth = nth.setScale(floatingPoint, RoundingMode.HALF_UP);
        nth = ONE.divide(SIXTEEN.pow(k, mathContext), mathContext).multiply
                (FOUR.divide(K8.add(ONE), mathContext).subtract
                (TWO.divide(K8.add(FOUR), mathContext)).subtract
                (ONE.divide(K8.add(FIVE), mathContext)).subtract
                (ONE.divide(K8.add(SIX), mathContext)));
        PI.addToPI(nth);
    }
}
