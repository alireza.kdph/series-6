package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.*;

public class PI {
    private static int floatingPoint = PICalculator.getFloatingPoint() + 1;
    private static final MathContext mathContext = new MathContext(floatingPoint, RoundingMode.HALF_UP);
    private static BigDecimal pi = new BigDecimal(0, mathContext);
    private static final ExecutorService executor = Executors.newFixedThreadPool(3);

    public static void manage() throws InterruptedException {
        for (int i = 0; i < floatingPoint + 5; i++) {
            Runnable runnable = new Calculator(i);
            executor.execute(runnable);
        }
        executor.shutdown();
        while (!executor.isTerminated()){}
    }

    synchronized public static void addToPI(BigDecimal nth) {
        pi = pi.add(nth, mathContext);
    }

    public static BigDecimal getPi() {
        return pi;
    }
}
