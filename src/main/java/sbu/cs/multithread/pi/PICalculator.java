package sbu.cs.multithread.pi;


public class PICalculator {
    private static int floatingPoint = 0;
    /**
     * calculate pi and represent it as string with given floating point number (numbers after .)
     * check test cases for more info
     * check pi with 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return pi in string format
     */
    public String calculate(int floatingPoint) {
        this.floatingPoint = floatingPoint + 5;
        try {
            PI pi = new PI();
            pi.manage();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return PI.getPi().toString().substring(0, floatingPoint + 2);
    }

    public static int getFloatingPoint() {
        return floatingPoint;
    }
}
